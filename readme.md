﻿## Installation
You have to install .NET Core 3.1 on your machine https://dotnet.microsoft.com/download

Insatall .NET Core CLI Runtime

Go to project root folder and run command below :
```bash
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
```

Insatall .NET Core CLI Tools

Go to project root folder and run command below :
```bash
dotnet add package Microsoft.EntityFrameworkCore.Design
```

## Docker
You have to install docker on your machine
https://docs.docker.com/install/


Go to project root folder and run command below

(this will create an instance of MSSQL Server
(container name : "cms_mssql") )

```
docker-compose -f docker-compose-local.yml up --build
```

## Database

Create a database. some tables and seeding data

Go to project root folder and run command below
```
dotnet ef database update
```

## Build react component
You have to install node and npm on your machine

Go to project root folder and run command below
```
    npm install
    npm run prod
```

## Run Project "prjCMS"
Go to project root folder and run command below

```
dotnet run
```

open browser and go to "http://localhost:5000"

1. username : admin, password : test123 (Administrator)
2. username : student, password : test123 (Student)