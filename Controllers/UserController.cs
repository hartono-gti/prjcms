﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using prjCMS.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using prjCMS.Models.Repos;
using prjCMS.Models.Views.User;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace prjCMS.Controllers
{    
    [Route("User")]
    public class UserController : Controller
    {
        private UserRepo _userRepo;
        private readonly DatabaseContext _context;
        public UserController(DatabaseContext pContext)
        {
            this._context = pContext;
            this._userRepo = new UserRepo(this._context);
        }

        [HttpGet]
        [Route("Login")]
        public IActionResult Login()
        {
            String returnUrl = HttpContext.Request.Query["ReturnUrl"].ToString();
            TempData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [Route("Login")]    
        public async Task<IActionResult> Login(UserLoginViewModel pUserLogin)
        {
            if (ModelState.IsValid)
            {
                var user = _userRepo.getUserByUsername(pUserLogin.Username);
                if (user != null) {
                    if(BCrypt.Net.BCrypt.Verify(pUserLogin.Password, user.Password))
                    {
                        var claims = new List<Claim>
                            {
                                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                                new Claim(ClaimTypes.Name, user.Name),
                                new Claim("Username", user.Username),
                                new Claim(ClaimTypes.Role, user.RoleId.ToString()),
                            };

                        var claimsIdentity = new ClaimsIdentity(
                            claims, CookieAuthenticationDefaults.AuthenticationScheme);

                        var authProperties = new AuthenticationProperties
                        {
                            //IsPersistent = true,
                            //ExpiresUtc = DateTime.UtcNow.AddMinutes(1)
                        };

                        await HttpContext.SignInAsync(
                            CookieAuthenticationDefaults.AuthenticationScheme,
                            new ClaimsPrincipal(claimsIdentity));

                        String returnUrl = TempData["ReturnUrl"].ToString();
                        if (!String.IsNullOrEmpty(returnUrl)) {
                            return LocalRedirect(returnUrl);
                        }
                        else {
                            return  RedirectToAction("Index", "UserShape");
                        }                        
                    }
                    else { TempData["error_message"] = "Invalid username or passowrd."; }
                }
                else TempData["error_message"] = "Invalid username or passowrd."; 
            }
            else TempData["error_message"] = "Invalid username or passowrd.";
            
            return View(pUserLogin);
        }

        [HttpGet]
        [Route("Logout")]
        public async Task<IActionResult> Logout() {
            await HttpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Login", "User");
        }

        [Authorize]
        [HttpGet]
        [Route("Index")]        
        public ActionResult Index()
        {
            UserCreateViewModel userCreate = new UserCreateViewModel(this._context);
            userCreate.Roles.Insert(0, new SelectListItem("<select role>", ""));
            userCreate.Role = "1";

            return View(userCreate);
        }

        [Authorize]
        [HttpPost]
        [Route("DataTable")]
        public ActionResult DataTable()
        {
            String editUrl = Url.Action("Edit", "User", new { }, Url.ActionContext.HttpContext.Request.Scheme);
            String deleteUrl = Url.Action("Delete", "User", new { }, Url.ActionContext.HttpContext.Request.Scheme);
            var users = _userRepo.getUserListForDataTable(HttpContext.Request, editUrl, deleteUrl);
            
            return Json(users);
        }

        [Authorize]
        [HttpPost]
        [Route("Create")]
        public ActionResult Create(UserCreateViewModel pUserCreate)
        {
            if (ModelState.IsValid)
            {                
                int loginUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                try
                {
                    var user = _userRepo.getUserByUsername(pUserCreate.Username);
                    if (user == null)
                    {
                        _userRepo.save(pUserCreate, loginUserId);
                        TempData["success_message"] = "Create user success.";
                    }
                    else
                    {
                        TempData["error_message"] = "Username already exists.";
                    }
                }
                catch (Exception) {
                    TempData["error_message"] = "Create user failed.";
                }               
            }
            else
            {
                TempData["error_message"] = "Invalid data.";
            }

            return RedirectToAction("Index", "User");
        }

        [HttpGet]
        [Route("Signup")]
        public ActionResult Signup()
        {
            UserCreateViewModel userCreate = new UserCreateViewModel();

            return View(userCreate);
        }

        [HttpPost]
        [Route("Signup")]
        public ActionResult Signup(UserCreateViewModel pUserCreate)
        {
            pUserCreate.Role = "2";
            ModelState.Clear();
            if (TryValidateModel(pUserCreate, nameof(pUserCreate)))
            {
                int userId = 1;
                try
                {
                    var user = _userRepo.getUserByUsername(pUserCreate.Username);
                    if (user == null)
                    {
                        _userRepo.save(pUserCreate, userId);
                        TempData["success_message"] = "Signup succes,  please login.";
                        return RedirectToAction("Login", "User");
                    }
                    else
                    {
                        TempData["error_message"] = "Username already exists.";
                        return View(pUserCreate);
                    }
                }
                catch (Exception)
                {
                    TempData["error_message"] = "Create user failed.";
                    return View(pUserCreate);
                }
            }
            else
            {
                TempData["error_message"] = "Invalid data.";
                return View(pUserCreate);
            }            
        }

        [Authorize]
        [HttpGet]
        [Route("Edit/{pId}")]
        public ActionResult Edit(int pId)
        {
            UserEditViewModel userEdit = _userRepo.getUserEditViewwModelById(pId);
            userEdit.Roles.Insert(0, new SelectListItem("<select role>", ""));

            return PartialView("_EditUserPartial", userEdit);
        }

        [Authorize]
        [ValidateAntiForgeryToken]
        [HttpPost]
        [Route("Edit/{pId}")]
        public ActionResult Edit(UserEditViewModel pUserEdit, int pId)
        {
            if (ModelState.IsValid)
            {
                int loginUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                try
                {
                    var user = _userRepo.getUserByUsername(pUserEdit.Username);
                    if (user == null || (user != null && user.Id == pId))
                    {
                        _userRepo.save(pUserEdit, pId, loginUserId);
                        TempData["success_message"] = "Update user success.";
                    }
                    else
                    {
                        TempData["error_message"] = "Update user failed, Username already exists.";
                    }
                }
                catch (Exception)
                {
                    TempData["error_message"] = "Update user failed.";
                }
            }
            else
            {
                TempData["error_message"] = "Invalid data.";
            }

            return RedirectToAction("Index", "User");
        }

        [HttpPost]
        [Route("Delete/{pId}")]
        public ActionResult Delete(int pId)
        {
            try
            {
                _userRepo.delete(pId);
                TempData["success_message"] = "Delete user success.";
            }
            catch (Exception)
            {
                TempData["error_message"] = "Delete user failed.";
            }

            return RedirectToAction("Index", "User");
        }
    }   
}