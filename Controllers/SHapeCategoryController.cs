﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using prjCMS.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using prjCMS.Models.Repos;
using prjCMS.Models.Views.User;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace prjCMS.Controllers
{
    [Authorize]
    [Route("ShapeCategory")]
    public class ShapeCategoryController : Controller
    {
        private ShapeCategoryRepo _shapeCategoryRepo;
        private readonly DatabaseContext _context;
        public ShapeCategoryController(DatabaseContext pContext)
        {
            this._context = pContext;
            this._shapeCategoryRepo = new ShapeCategoryRepo(this._context);
        }

        [HttpGet]
        [Route("Index")]        
        public ActionResult Index()
        {
            ShapeCateogry shapeCategory = new ShapeCateogry();            
            return View(shapeCategory);
        }

        [HttpPost]
        [Route("DataTable")]
        public ActionResult DataTable()
        {
            String editUrl = Url.Action("Edit", "ShapeCategory", new { }, Url.ActionContext.HttpContext.Request.Scheme);
            String deleteUrl = Url.Action("Delete", "ShapeCategory", new { }, Url.ActionContext.HttpContext.Request.Scheme);
            var shapeCategories = _shapeCategoryRepo.getShapeCategoryListForDataTable(HttpContext.Request, editUrl, deleteUrl);
            
            return Json(shapeCategories);
        }

        [Authorize]
        [HttpPost]
        [Route("Create")]
        public ActionResult Create(ShapeCateogry pShapeCategory)
        {
            if (ModelState.IsValid)
            {
                int loginUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                try
                {
                    _shapeCategoryRepo.save(pShapeCategory, loginUserId);
                    TempData["success_message"] = "Create shape category success.";
                }
                catch (Exception) {
                    TempData["error_message"] = "Create shape category failed.";
                }               
            }
            else
            {
                TempData["error_message"] = "Invalid data.";
            }

            return RedirectToAction("Index", "ShapeCategory");
        }

        [HttpGet]
        [Route("Edit/{pId}")]
        public ActionResult Edit(int pId)
        {
            ShapeCateogry shapeCateogry = _shapeCategoryRepo.getShapeCategoryById(pId);

            return PartialView("_EditShapeCategoryPartial", shapeCateogry);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [Route("Edit/{pId}")]
        public ActionResult Edit(ShapeCateogry pShapeCategory, int pId)
        {
            if (ModelState.IsValid)
            {
                int loginUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                try
                {
                    _shapeCategoryRepo.save(pShapeCategory, pId, loginUserId);
                    TempData["success_message"] = "Update shape category success.";
                }
                catch (Exception)
                {
                    TempData["error_message"] = "Update shape category failed.";
                }
            }
            else
            {
                TempData["error_message"] = "Invalid data.";
            }

            return RedirectToAction("Index", "ShapeCategory");
        }

        [HttpPost]
        [Route("Delete/{pId}")]
        public ActionResult Delete(int pId)
        {
            try
            {
                _shapeCategoryRepo.delete(pId);
                TempData["success_message"] = "Delete shape category success.";
            }
            catch (Exception)
            {
                TempData["error_message"] = "Delete shape category failed.";
            }

            return RedirectToAction("Index", "ShapeCategory");
        }
    }   
}