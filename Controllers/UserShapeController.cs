﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using prjCMS.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using prjCMS.Models.Repos;
using prjCMS.Models.Views.User;
using Microsoft.AspNetCore.Mvc.Rendering;
using prjCMS.Models.Views.UserShape;

namespace prjCMS.Controllers
{
    [Authorize]
    [Route("")]
    [Route("UserShape")]
    public class UserShapeController : Controller
    {
        private UserShapeRepo _userShapeRepo;
        private readonly DatabaseContext _context;
        public UserShapeController(DatabaseContext pContext)
        {
            this._context = pContext;
            this._userShapeRepo = new UserShapeRepo(this._context);
        }

        [HttpGet]
        [Route("/")]
        [Route("Index")]        
        public ActionResult Index()
        {
            String shapeCategoryParameterUrl = Url.Action("Parameters", "UserShape", new { }, Url.ActionContext.HttpContext.Request.Scheme);
            UserShapeCreateViewModel userShapeCreate = new UserShapeCreateViewModel(this._context);

            userShapeCreate.ShapeCategories.Insert(0, new SelectListItem("<select shape category>", ""));
            ViewBag.shapeCategoryParameterUrl = shapeCategoryParameterUrl;

            return View(userShapeCreate);
        }

        [HttpPost]
        [Route("DataTable")]
        public ActionResult DataTable()
        {
            int loginUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
            int roleId = int.Parse(User.FindFirstValue(ClaimTypes.Role));
            String editUrl = Url.Action("Edit", "UserShape", new { }, Url.ActionContext.HttpContext.Request.Scheme);
            String deleteUrl = Url.Action("Delete", "UserShape", new { }, Url.ActionContext.HttpContext.Request.Scheme);

            var shapeCategories = _userShapeRepo.geUserShapeListForDataTable(HttpContext.Request, editUrl, deleteUrl, roleId, loginUserId);
            
            return Json(shapeCategories);
        }

        [HttpGet]
        [Route("Parameters")]
        public ActionResult ShapeCategoryParameter(int pId)
        {
            ShapeCategoryRepo shapeCategoryRepo = new ShapeCategoryRepo(_context);
            ShapeCateogry shapeCategory = shapeCategoryRepo.getShapeCategoryById(pId);

            String[] shapeCategoryParameters = shapeCategory.RequiredParameter.Split(new char[] { ',' });            
            ViewBag.shapeCategoryParameters = shapeCategoryParameters;

            return PartialView("_ShapeCategoryParameterPartial");
        }

        [Authorize]
        [HttpPost]
        [Route("Create")]
        public ActionResult Create()
        {
            
            int loginUserId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

            try
            {
                _userShapeRepo.save(HttpContext.Request, loginUserId);
                TempData["success_message"] = "Create user shape success.";
            }
            catch (Exception)
            {
                TempData["error_message"] = "Create user shape failed.";
            }

            return RedirectToAction("Index", "UserShape");
        }

        [HttpPost]
        [Route("Delete/{pId}")]
        public ActionResult Delete(int pId)
        {
            try
            {
                _userShapeRepo.delete(pId);
                TempData["success_message"] = "Delete user shape success.";
            }
            catch (Exception)
            {
                TempData["error_message"] = "Delete user shape failed.";
            }

            return RedirectToAction("Index", "UserShape");
        }
    }   
}