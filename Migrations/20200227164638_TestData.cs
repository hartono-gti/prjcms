﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace prjCMS.Migrations
{
    public partial class TestData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //roles
            migrationBuilder.Sql("INSERT INTO roles (Name, CreatedAt, CreatedBy, LastUpdatedAt, LastUpdatedBy)" +
                "VALUES('Administrator', GETDATE(), 1, GETDATE(), 1)");
            migrationBuilder.Sql("INSERT INTO roles (Name, CreatedAt, CreatedBy, LastUpdatedAt, LastUpdatedBy)" +
                "VALUES('Student', GETDATE(), 1, GETDATE(), 1)");

            //users
            String password = "test123";
            password = BCrypt.Net.BCrypt.HashPassword(password);
            migrationBuilder.Sql("INSERT INTO users (Name, Username, Password, RoleId, CreatedAt, CreatedBy, LastUpdatedAt, LastUpdatedBy)" +
                "VALUES('Admin_1', 'admin', '" + password + "', 1, GETDATE(), 1, GETDATE(), 1)");
            migrationBuilder.Sql("INSERT INTO users (Name, Username, Password, RoleId, CreatedAt, CreatedBy, LastUpdatedAt, LastUpdatedBy)" +
                "VALUES('Student_1', 'student', '" + password + "', 2, GETDATE(), 1, GETDATE(), 1)");

            //shape_categories
            migrationBuilder.Sql("INSERT INTO shape_categories (Name, RequiredParameter, AreaFormula, CreatedAt, CreatedBy, LastUpdatedAt, LastUpdatedBy)" +
                "VALUES('Square', 'S', 'S * S', GETDATE(), 1, GETDATE(), 1)");
            migrationBuilder.Sql("INSERT INTO shape_categories (Name, RequiredParameter, AreaFormula, CreatedAt, CreatedBy, LastUpdatedAt, LastUpdatedBy)" +
                "VALUES('Rectangle', 'W, L', 'W * L', GETDATE(), 1, GETDATE(), 1)");
            migrationBuilder.Sql("INSERT INTO shape_categories (Name, RequiredParameter, AreaFormula, CreatedAt, CreatedBy, LastUpdatedAt, LastUpdatedBy)" +
                "VALUES('Triangle', 'B, H', '0.5 * B * H', GETDATE(), 1, GETDATE(), 1)");

            //user_shapes
            migrationBuilder.Sql("INSERT INTO user_shapes (Name, ShapeCategoryId, RequiredParameter, RequiredParameterValue, AreaFormula, AreaValue, CreatedAt, CreatedBy, LastUpdatedAt, LastUpdatedBy)" +
                "VALUES('Square', 1, 'S', 5, 'S * S', 25, GETDATE(), 1, GETDATE(), 1)");
            migrationBuilder.Sql("INSERT INTO user_shapes (Name, ShapeCategoryId, RequiredParameter, RequiredParameterValue, AreaFormula, AreaValue, CreatedAt, CreatedBy, LastUpdatedAt, LastUpdatedBy)" +
                "VALUES('Rectangle', 2, 'W, L', '5, 8', 'W * L', 40, GETDATE(), 1, GETDATE(), 1)");
            migrationBuilder.Sql("INSERT INTO user_shapes (Name, ShapeCategoryId, RequiredParameter, RequiredParameterValue, AreaFormula, AreaValue, CreatedAt, CreatedBy, LastUpdatedAt, LastUpdatedBy)" +
                "VALUES('Rectangle', 2, 'W, L', '5, 8', 'W * L', 40, GETDATE(), 2, GETDATE(), 2)");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("TRUNCATE TABLE user_shapes");
            migrationBuilder.Sql("TRUNCATE TABLE shape_categories");
            migrationBuilder.Sql("TRUNCATE TABLE users");
            migrationBuilder.Sql("TRUNCATE TABLE roles");
        }
    }
}
