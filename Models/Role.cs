﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace prjCMS.Models
{
    [Table("roles", Schema="dbo")]
    public class Role
    {
        public Role(){            
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }

        [StringLength(30)]
        public String Name { get; set; }        

        //Users
        [InverseProperty("RoleId")]
        public List<User> Users;

        public DateTime CreatedAt { get; set; }
        public Int32 CreatedBy { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
        public Int32? LastUpdatedBy { get; set; }      
    }
}

