﻿using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using prjCMS.Models.Helpers;
using prjCMS.Models.Views.User;

namespace prjCMS.Models.Repos
{
    public class UserRepo
    {
        private readonly DatabaseContext _context;

        public UserRepo(DatabaseContext pContext)
        {
            this._context = pContext;
        }

        public User getUserById(int pId)
        {
            return _context.Users.FirstOrDefault(u => u.Id == pId);
        }

        public UserEditViewModel getUserEditViewwModelById(int pId)
        {
            UserEditViewModel userEdit = new UserEditViewModel(this._context);
            User user = this.getUserById(pId);
            userEdit.Id = user.Id;
            userEdit.Name = user.Name;
            userEdit.Username = user.Username;
            userEdit.Role = user.RoleId.ToString();
            return userEdit;
        }

        public User getUserByUsername(String pUsername)
        {
            return _context.Users.FirstOrDefault(u => u.Username == pUsername);
        }

        public dynamic getUserListForDataTable(HttpRequest pRequest, String pEditUrl, String pDeleteUrl)
        {
            var dataTableParam = DataTableHelper.getDataTableParameter(pRequest);
            var users =
                (from user in _context.Users
                    select new
                    {   
                        Name = user.Name,
                        Username = user.Username,
                        Role = user.Role.Name,
                        Action =
                        "<a href='" + pEditUrl + "/" + user.Id + "' class='btn-sm btn-info btnEditUser'>" +
                            "<i class='fa fa-edit'></i>" +
                                "Edit" +
                        "</a>" +
                        "&nbsp; &nbsp;" +
                        "<a href='" + pDeleteUrl + "/" + user.Id + "' class='btn-sm btn-danger btnDeleteUser' data-name='" + user.Name+ "' data-toggle='modal' data-target=#deleteModal>" +
                            "<i class='fa fa-trash'></i>" +
                                "Delete" +
                        "</button>"

                    }
                );
            
            //Sorting
            if (!(string.IsNullOrEmpty(dataTableParam.sortColumn.ToString()) && string.IsNullOrEmpty(dataTableParam.sortColumnDirection)))
            {
                if(dataTableParam.sortColumnDirection.ToLower() == "asc") {
                    if (dataTableParam.sortColumn.ToLower() == "name")
                        users = users.OrderBy(u => u.Name);
                    else if (dataTableParam.sortColumn.ToLower() == "username")
                        users = users.OrderBy(u => u.Username);
                    else if (dataTableParam.sortColumn.ToLower() == "role")
                        users = users.OrderBy(u => u.Role);
                }
                else
                {
                    if (dataTableParam.sortColumn.ToLower() == "name")
                        users = users.OrderByDescending(u => u.Name);
                    else if (dataTableParam.sortColumn.ToLower() == "username")
                        users = users.OrderByDescending(u => u.Username);
                    else if (dataTableParam.sortColumn.ToLower() == "role")
                        users = users.OrderByDescending(u => u.Role);
                }
            }

            ////Search  
            if (!string.IsNullOrEmpty(dataTableParam.searchValue))
            {
                String searchvalue = dataTableParam.searchValue;
                users = users.Where(u => u.Username.Contains(searchvalue));
            }

            //total number of rows count   
            int recordsTotal = users.Count();
            int skip = dataTableParam.skip;
            int pageSize = dataTableParam.pageSize;
            var data = users.Skip(skip).Take(pageSize).ToList();            

            return new { draw = dataTableParam.draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
        }

        public void save(UserCreateViewModel pUserCreate, int pCreatedBy)
        {
            User user = new User();
            user.Name = pUserCreate.Name;
            user.Password = BCrypt.Net.BCrypt.HashPassword(pUserCreate.Password);
            user.Username = pUserCreate.Username;
            user.RoleId = int.Parse(pUserCreate.Role);
            user.CreatedBy = pCreatedBy;
            user.CreatedAt = DateTime.Now;
            user.LastUpdatedBy = pCreatedBy;
            user.LastUpdatedAt = DateTime.Now;

            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public void save(UserEditViewModel pUserEdit, int pUserId, int pUpdatedBy)
        {
            User user = this.getUserById(pUserId);
            user.Name = pUserEdit.Name;
            user.Username = pUserEdit.Username;
            user.RoleId = int.Parse(pUserEdit.Role);
            user.LastUpdatedBy = pUpdatedBy;
            user.LastUpdatedAt = DateTime.Now;

            _context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();
        }

        public void delete(int pUserId)
        {
            User user = this.getUserById(pUserId);
            _context.Users.Remove(user);
            _context.SaveChanges();
        }
    }
}
