﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using prjCMS.Models.Helpers;
using prjCMS.Models.Views.User;

namespace prjCMS.Models.Repos
{
    public class ShapeCategoryRepo
    {
        private readonly DatabaseContext _context;

        public ShapeCategoryRepo(DatabaseContext pContext)
        {
            this._context = pContext;
        }

        public ShapeCateogry getShapeCategoryById(int pId)
        {
            return _context.ShapeCategories.FirstOrDefault(u => u.Id == pId);
        }

        public List<ShapeCateogry> getShapeCategoryList()
        {
            return _context.ShapeCategories.ToList();
        }

        public dynamic getShapeCategoryListForDataTable(HttpRequest pRequest, String pEditUrl, String pDeleteUrl)
        {
            var dataTableParam = DataTableHelper.getDataTableParameter(pRequest);
            var shapeCategories =
                (from shape in _context.ShapeCategories
                    select new
                    {   
                        Name = shape.Name,
                        RequiredParameter = shape.RequiredParameter,
                        AreaFormula = shape.AreaFormula,
                        Action =
                        "<a href='" + pEditUrl + "/" + shape.Id + "' class='btn-sm btn-info btnEditShapeCategory'>" +
                            "<i class='fa fa-edit'></i>" +
                                "Edit" +
                        "</a>" +
                        "&nbsp; &nbsp;" +
                        "<a href='" + pDeleteUrl + "/" + shape.Id + "' class='btn-sm btn-danger btnDeleteShapeCategory' data-name='" + shape.Name+ "' data-toggle='modal' data-target=#deleteModal>" +
                            "<i class='fa fa-trash'></i>" +
                                "Delete" +
                        "</button>"

                    }
                );
            
            //Sorting
            if (!(string.IsNullOrEmpty(dataTableParam.sortColumn.ToString()) && string.IsNullOrEmpty(dataTableParam.sortColumnDirection)))
            {
                if(dataTableParam.sortColumnDirection.ToLower() == "asc") {
                    if (dataTableParam.sortColumn.ToLower() == "name")
                        shapeCategories = shapeCategories.OrderBy(u => u.Name);
                    else if (dataTableParam.sortColumn.ToLower() == "requiredparameter")
                        shapeCategories = shapeCategories.OrderBy(u => u.RequiredParameter);
                    else if (dataTableParam.sortColumn.ToLower() == "areaformula")
                        shapeCategories = shapeCategories.OrderBy(u => u.AreaFormula);
                }
                else
                {
                    if (dataTableParam.sortColumn.ToLower() == "name")
                        shapeCategories = shapeCategories.OrderByDescending(u => u.Name);
                    else if (dataTableParam.sortColumn.ToLower() == "requiredparameter")
                        shapeCategories = shapeCategories.OrderByDescending(u => u.RequiredParameter);
                    else if (dataTableParam.sortColumn.ToLower() == "areaformula")
                        shapeCategories = shapeCategories.OrderByDescending(u => u.AreaFormula);
                }
            }

            ////Search  
            if (!string.IsNullOrEmpty(dataTableParam.searchValue))
            {
                String searchvalue = dataTableParam.searchValue;
                shapeCategories = shapeCategories.Where(u => u.Name.Contains(searchvalue));
            }

            //total number of rows count   
            int recordsTotal = shapeCategories.Count();
            int skip = dataTableParam.skip;
            int pageSize = dataTableParam.pageSize;
            var data = shapeCategories.Skip(skip).Take(pageSize).ToList();            

            return new { draw = dataTableParam.draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
        }

        public void save(ShapeCateogry pShapeCategory, int pCreatedBy)
        {
            pShapeCategory.CreatedBy = pCreatedBy;
            pShapeCategory.CreatedAt = DateTime.Now;
            pShapeCategory.LastUpdatedBy = pCreatedBy;
            pShapeCategory.LastUpdatedAt = DateTime.Now;

            _context.ShapeCategories.Add(pShapeCategory);
            _context.SaveChanges();
        }

        public void save(ShapeCateogry pShapeCategory, int pShapeCategoryId, int pUpdatedBy)
        {
            ShapeCateogry shapeCategory = this.getShapeCategoryById(pShapeCategoryId);
            shapeCategory.Name = pShapeCategory.Name;
            shapeCategory.RequiredParameter = pShapeCategory.RequiredParameter;
            shapeCategory.AreaFormula = pShapeCategory.AreaFormula;
            shapeCategory.LastUpdatedBy = pUpdatedBy;
            shapeCategory.LastUpdatedAt = DateTime.Now;

            _context.Entry(shapeCategory).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();
        }

        public void delete(int pShapeCategoryId)
        {
            ShapeCateogry shapeCategory = this.getShapeCategoryById(pShapeCategoryId);
            _context.ShapeCategories.Remove(shapeCategory);
            _context.SaveChanges();
        }
    }
}
