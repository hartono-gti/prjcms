﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace prjCMS.Models.Repos
{
    public class RoleRepo
    {
        private readonly DatabaseContext _context;

        public RoleRepo(DatabaseContext pContext)
        {
            this._context = pContext;
        }

        public List<Role> getRoleList()
        {
            return _context.Roles.ToList();
        }
    }
}
