﻿using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using prjCMS.Models.Helpers;
using prjCMS.Models.Views.User;
using prjCMS.Models;
using System.Data;

namespace prjCMS.Models.Repos
{
    public class UserShapeRepo
    {
        private readonly DatabaseContext _context;

        public UserShapeRepo(DatabaseContext pContext)
        {
            this._context = pContext;
        }

        public UserShape getUserShapeById(int pId)
        {
            return _context.UserShapes.FirstOrDefault(u => u.Id == pId);
        }

        public dynamic geUserShapeListForDataTable(HttpRequest pRequest, String pEditUrl, String pDeleteUrl, Int32 pRoleId, Int32 pUserId)
        {
            var dataTableParam = DataTableHelper.getDataTableParameter(pRequest);

            var userShapes = 
                (from userShape in _context.UserShapes
                 select new
                 {
                     Name = userShape.Name,
                     RequiredParameter = userShape.RequiredParameter,
                     AreaFormula = userShape.AreaFormula,
                     RequiredParameterValue = userShape.RequiredParameterValue,
                     AreaValue = userShape.AreaValue,
                     CreatedBy = userShape.CreatedByUser.Name,
                     Action =
                     "<a href='" + pDeleteUrl + "/" + userShape.Id + "' class='btn-sm btn-danger btnDeleteUserShape' data-name='" + userShape.Name + "' data-toggle='modal' data-target=#deleteModal>" +
                         "<i class='fa fa-trash'></i>" +
                             "Delete" +
                     "</button>",
                     CreatedByUserId = userShape.CreatedBy
                 }
                );

            //Sorting
            if (!(string.IsNullOrEmpty(dataTableParam.sortColumn.ToString()) && string.IsNullOrEmpty(dataTableParam.sortColumnDirection)))
            {
                if (dataTableParam.sortColumnDirection.ToLower() == "asc")
                {
                    if (dataTableParam.sortColumn.ToLower() == "name")
                        userShapes = userShapes.OrderBy(u => u.Name);
                    else if (dataTableParam.sortColumn.ToLower() == "requiredparameter")
                        userShapes = userShapes.OrderBy(u => u.RequiredParameter);
                    else if (dataTableParam.sortColumn.ToLower() == "requiredparametervalue")
                        userShapes = userShapes.OrderBy(u => u.RequiredParameterValue);
                    else if (dataTableParam.sortColumn.ToLower() == "areaformula")
                        userShapes = userShapes.OrderBy(u => u.AreaFormula);
                    else if (dataTableParam.sortColumn.ToLower() == "areavalue")
                        userShapes = userShapes.OrderBy(u => u.AreaValue);
                }
                else
                {
                    if (dataTableParam.sortColumn.ToLower() == "name")
                        userShapes = userShapes.OrderByDescending(u => u.Name);
                    else if (dataTableParam.sortColumn.ToLower() == "requiredparameter")
                        userShapes = userShapes.OrderByDescending(u => u.RequiredParameter);
                    else if (dataTableParam.sortColumn.ToLower() == "requiredparametervalue")
                        userShapes = userShapes.OrderByDescending(u => u.RequiredParameterValue);
                    else if (dataTableParam.sortColumn.ToLower() == "areaformula")
                        userShapes = userShapes.OrderByDescending(u => u.AreaFormula);
                    else if (dataTableParam.sortColumn.ToLower() == "areavalue")
                        userShapes = userShapes.OrderByDescending(u => u.AreaValue);
                }
            }

            ////Search  
            if (!string.IsNullOrEmpty(dataTableParam.searchValue))
            {
                String searchvalue = dataTableParam.searchValue;
                userShapes = userShapes.Where(u => u.Name.Contains(searchvalue));
            }

            if (pRoleId != 1) userShapes = userShapes.Where(u => u.CreatedByUserId == pUserId);

            //total number of rows count   
            int recordsTotal = userShapes.Count();
            int skip = dataTableParam.skip;
            int pageSize = dataTableParam.pageSize;
            var data = userShapes.Skip(skip).Take(pageSize).ToList();

            return new { draw = dataTableParam.draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
        }

        public void save(HttpRequest pRequest , int pCreatedBy)
        {
            /*
             [0] => Name
             [1] => ShapeCategory
             [...] => parameter
             [m] => RequestVerificationToken
             */

            UserShape userShape = new UserShape();
            ShapeCategoryRepo shapeCategoryRepo = new ShapeCategoryRepo(_context);

            Int32 shapeCategoryId = Int32.Parse(pRequest.Form["ShapeCategory"]);
            String name = pRequest.Form["Name"];
            ShapeCateogry shapeCategory = shapeCategoryRepo.getShapeCategoryById(shapeCategoryId);
            String[] parameters = shapeCategory.RequiredParameter.Split(new char[] { ',' });
            String areaFormula = shapeCategory.AreaFormula;
            String parameterValues = "";
            String parameter = "";

            for(int i=0; i< parameters.Length; i++) {
                parameter = parameters[i].Trim();
                parameterValues += pRequest.Form[parameter] + ", ";
                areaFormula = areaFormula.Replace(parameter, pRequest.Form[parameter]);
            }
            parameterValues = parameterValues.Substring(0, parameterValues.Length - 2);

            DataTable dt = new DataTable();
            var areaValue = dt.Compute(areaFormula, "");


            userShape.Name = name;
            userShape.ShapeCategoryId = shapeCategoryId;
            userShape.RequiredParameter = shapeCategory.RequiredParameter;
            userShape.RequiredParameterValue = parameterValues;
            userShape.AreaFormula = shapeCategory.AreaFormula;
            userShape.AreaValue =  double.Parse(areaValue.ToString());
            userShape.CreatedBy = pCreatedBy;
            userShape.CreatedAt = DateTime.Now;
            userShape.LastUpdatedBy = pCreatedBy;
            userShape.LastUpdatedAt = DateTime.Now;

            _context.UserShapes.Add(userShape);
            _context.SaveChanges();
        }        

        public void delete(int pId)
        {
            UserShape userShape = this.getUserShapeById(pId);
            _context.UserShapes.Remove(userShape);
            _context.SaveChanges();
        }
    }
}
