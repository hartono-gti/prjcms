﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace prjCMS.Models.Views.User
{
    public class UserLoginViewModel
    {
        public UserLoginViewModel(){}

        [Required (ErrorMessage = "Username is required.")]
        public String Username { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [DataType(DataType.Password)]
        public String Password { get; set; }
    }
}