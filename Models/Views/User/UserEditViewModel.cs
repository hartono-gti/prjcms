﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using prjCMS.Models.Repos;

namespace prjCMS.Models.Views.User
{
    public class UserEditViewModel
    {
        protected DatabaseContext _context;
        public UserEditViewModel() { }
        public UserEditViewModel(DatabaseContext pContext) {
            this._context = pContext;
            this.Roles = this.getRoles();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required.")]
        [MinLength(3, ErrorMessage = "Minimum 3 characters.")]
        public String Name { get; set; }

        [Required(ErrorMessage = "Username is required.")]
        [MinLength(3, ErrorMessage = "Minimum 3 characters.")]
        public String Username { get; set; }

        [Required(ErrorMessage = "Role is required.")]
        public string Role { get; set; }

        public List<SelectListItem> Roles { get; }

        public List<SelectListItem>getRoles(){
            RoleRepo roleRepo = new RoleRepo(this._context);
            List<Role> roleList = roleRepo.getRoleList();
            List<SelectListItem> roleListItem = new List<SelectListItem>();
            SelectListItem roleItem;

            for (int i=0; i< roleList.Count; i++) {
                roleItem = new SelectListItem
                {
                    Value = roleList[i].Id.ToString(),
                    Text = roleList[i].Name
                };

                roleListItem.Add(roleItem);
            }

            return roleListItem;
        }
    }
}
