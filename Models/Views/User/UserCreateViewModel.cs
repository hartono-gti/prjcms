﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using prjCMS.Models.Repos;

namespace prjCMS.Models.Views.User
{
    public class UserCreateViewModel : UserEditViewModel
    {
        public UserCreateViewModel() : base() {}
        public UserCreateViewModel(DatabaseContext pContext) : base(pContext){}

        [Required(ErrorMessage = "Password is required.")]
        [MinLength(6, ErrorMessage = "Minimum 6 characters.")]
        [DataType(DataType.Password)]
        public String Password { get; set; }

        [Required(ErrorMessage = "Confirm password is required.")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Confirm password is not matched.")]
        public String Confirm { get; set; }

    }
}
