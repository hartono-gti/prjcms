﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.Rendering;
using prjCMS.Models.Repos;

namespace prjCMS.Models.Views.UserShape
{
    public class ShapeCategoryParameterViewModel
    {
        protected DatabaseContext _context;
        public ShapeCategoryParameterViewModel() { }
        public ShapeCategoryParameterViewModel(DatabaseContext pContext)
        {
            this._context = pContext;
            this.ShapeCategories = this.getShapeCategories();
        }

        public Int32 Id { get; set; }

        [Required(ErrorMessage = "Shape category is required.")]
        public string ShapeCateogry { get; set; }

        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }

        public List<SelectListItem> ShapeCategories { get; set; }

        private List<SelectListItem> getShapeCategories()
        {
            ShapeCategoryRepo shapeCategoryRepo = new ShapeCategoryRepo(this._context);
            List<ShapeCateogry> shapeCategoryList = shapeCategoryRepo.getShapeCategoryList();
            List<SelectListItem> spaeCategoryListItem = new List<SelectListItem>();
            SelectListItem shapeCategoryItem;

            for (int i = 0; i < shapeCategoryList.Count; i++)
            {
                shapeCategoryItem = new SelectListItem
                {
                    Value = shapeCategoryList[i].Id.ToString(),
                    Text = shapeCategoryList[i].Name
                };

                spaeCategoryListItem.Add(shapeCategoryItem);
            }

            return spaeCategoryListItem;
        }

    }
}
