﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace prjCMS.Models
{
    [Table("user_shapes", Schema="dbo")]
    public class UserShape
    {
        public UserShape(){            
        }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }

        [Required]
        [ForeignKey("UserShape_ShapeCategory")]
        public Int32 ShapeCategoryId { get; set; }
        public ShapeCateogry ShapeCategory { get; set; }

        [StringLength(30)]
        [Required]
        public String Name { get; set; }

        [StringLength(50)]
        [Required]
        public String RequiredParameter { get; set; }

        [StringLength(50)]
        [Required]
        public String RequiredParameterValue { get; set; }

        [StringLength(100)]
        [Required]
        public String AreaFormula { get; set; }

        public double AreaValue { get; set; }

        public DateTime CreatedAt { get; set; }        

        [ForeignKey("UserShape_User")]
        public Int32 CreatedBy { get; set; }
        public User CreatedByUser { get; set; }

        public DateTime? LastUpdatedAt { get; set; }
        public Int32? LastUpdatedBy { get; set; }

    }
}
