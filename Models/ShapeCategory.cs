﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace prjCMS.Models
{
    [Table("shape_categories", Schema="dbo")]
    public class ShapeCateogry
    {
        public ShapeCateogry(){            
        }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }

        [StringLength(30)]
        [Required]
        public String Name { get; set; }

        [StringLength(50)]
        [Required]
        public String RequiredParameter { get; set; }

        [StringLength(100)]
        [Required]
        public String AreaFormula { get; set; }

        //UserShapes
        [InverseProperty("CreatedByUserShape_User")]
        public List<UserShape> UserShapes;

        public DateTime CreatedAt { get; set; }
        public Int32 CreatedBy { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
        public Int32? LastUpdatedBy { get; set; }


    }
}
