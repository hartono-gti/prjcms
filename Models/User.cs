﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace prjCMS.Models
{
    [Table("users", Schema="dbo")]
    public class User
    {
        public User(){            
        }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }

        [Column("Name")]
        [StringLength(30)]
        [Required]
        public String Name { get; set; }

        [StringLength(30)]
        [Required]        
        public String Username { get; set; }

        [StringLength(100)]
        [Required]
        [DataType(DataType.Password)]
        public String Password { get; set; }

        [ForeignKey("RoleId")]
        public Int32 RoleId { get; set; }
        public Role Role { get; set; }

        //UserShapes
        [InverseProperty("IUserSahpe_User")]
        public List<UserShape> UserShapes;        

        public DateTime CreatedAt { get; set; }
        public Int32 CreatedBy { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
        public Int32? LastUpdatedBy { get; set; }


    }
}
