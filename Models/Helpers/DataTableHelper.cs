﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace prjCMS.Models.Helpers
{
    public class DataTableHelper
    {

        public DataTableHelper() { }

        public static dynamic getDataTableParameter(HttpRequest pRequest) {
            var draw = pRequest.Form["draw"].FirstOrDefault();
            var start = pRequest.Form["start"].FirstOrDefault();
            var length = pRequest.Form["length"].FirstOrDefault();
            var sortColumn = pRequest.Form["columns[" + pRequest.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = pRequest.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = pRequest.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            return 
            new
            {
                draw = draw,
                pageSize = pageSize,
                skip = skip,
                sortColumn = sortColumn,
                sortColumnDirection = sortColumnDirection,
                searchValue = searchValue
            };
        }
    }
}
