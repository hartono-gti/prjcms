﻿
using Microsoft.EntityFrameworkCore;

namespace prjCMS.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options) {
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<ShapeCateogry> ShapeCategories { get; set; }
        public DbSet<UserShape> UserShapes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //User
            modelBuilder.Entity<User>()
                .HasOne<Role>(m => m.Role) //User.RoleId            -> Role
                .WithMany()                //Role.Users             -> User
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>()
                .HasIndex(m => m.Username)
                .IsUnique()
                .HasName("IX_users_Username");

            //UserShape
            modelBuilder.Entity<UserShape>()
                .HasOne<ShapeCateogry>(m => m.ShapeCategory) //UserShape.ShapeCategoryId    -> ShapeCategory
                .WithMany()                                  //ShapeCateogry.UserShapes     -> UserShape
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<UserShape>()
                .HasOne<User>(m => m.CreatedByUser) //UserShape.CreatedBy    -> User
                .WithMany()                       //User.UserShapes        -> UserShape
                .HasForeignKey(m => m.CreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

        }
    }

}
