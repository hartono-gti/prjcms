﻿const path = require("path");
module.exports = {
    entry : {
        layout: "./wwwroot/scripts/src/shared/index.js",
        layout2: "./wwwroot/scripts/src/shared/index2.js",
        home_index: "./wwwroot/scripts/src/home/index.js",
        home_edit: "./wwwroot/scripts/src/home/edit.js",
    },
    output : {
        path : path.resolve(__dirname, "../dist"),
        filename : "[name].js"
    },
    module : {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-react']
                    }
                }
            },
            {
                test: /\.(scss)$/, 
                exclude: [/\module.(scss)$/, /node_modules/     ], //exclude created module scss and node_modules folder
                use: 
                [
                    {
                        loader: 'style-loader', // inject CSS to page
                    },{
                        loader: 'css-loader', // translates CSS into CommonJS modules
                        options: {
                        },
                    }, {
                        loader: 'postcss-loader', // Run post css actions
                        options: {
                            plugins: function () { // post css plugins, can be exported to postcss.config.js
                                return [
                                    require('precss'),
                                    require('autoprefixer')
                                    ];
                            }
                        }
                    }, {
                      loader: 'sass-loader' // compiles Sass to CSS
                    }
                ]
            },
            {
                test: /\module.(scss)$/, //only for created module scss [filename].module.scss
                include: [/scripts\/src/],
                use: 
                [
                    {
                        loader: 'style-loader', // inject CSS to page
                    },{
                        loader: 'css-loader', // translates CSS into CommonJS modules
                        options: {
                            modules: true,
                        },
                    }, {
                        loader: 'postcss-loader', // Run post css actions
                        options: {
                            plugins: function () { // post css plugins, can be exported to postcss.config.js
                                return [
                                    require('precss'),
                                    require('autoprefixer')
                                    ];
                            }
                        }
                    }, {
                      loader: 'sass-loader' // compiles Sass to CSS
                    }
                ]
            },
            {
                test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: '../../scripts/dist/fonts/'
                        }
                    }
                ]
     
            },
            {
                test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 10000,
                            name: '[name].[ext]',
                            outputPath: '../../scripts/dist/fonts/'
                        }
                    }
                ]
            },
        ]
    }
}
