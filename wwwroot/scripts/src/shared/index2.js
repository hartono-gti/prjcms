﻿import React from 'react';
import ReactDOM from 'react-dom';
import CSideBar from './component/csidebar';

import jquery from "jquery";
window.$ = jquery;
window.jQuery = jquery

import 'bootstrap';
import '../../../styles/bootstrap-custom.scss';
import "../../../styles/font-awesome.scss";
        
const menus =
    [
    ];

ReactDOM.render(<CSideBar menus={menus} />, document.getElementById('side'));

