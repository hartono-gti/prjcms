﻿import React from 'react';
import ReactDOM from 'react-dom';
import headerstyle from './cheader.module.scss'; // using module technique

class CHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>                
                <div className={headerstyle.header}>Shape CMS                    
                    <div className="float-right">                                                
                        <span className={headerstyle.logput}>
                            <i className="fa fa-user"></i>&nbsp;
                            {gName} &nbsp; | &nbsp;
                        </span>

                        <a href="/User/Logout" className={headerstyle.logput} >
                            <i className="fa fa-sign-out"></i>&nbsp;
                            Logout
                        </a>
                    </div>
                </div>
            </>
        );
    }
}

export default CHeader;