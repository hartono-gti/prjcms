﻿import React from 'react';
import ReactDOM from 'react-dom';
import CMenu from "./cmenu";

class CMenuWithSubMenu extends React.Component {
    constructor(props) {
        super(props);
    }    
        
    render(){
        const menu = this.props.menu;
        const menuId = this.props.menuId;
        const strMenuHRef = "#A" + menuId.toString();
        const strSubMenuId = "A" + menuId.toString();
        const subMenus = menu.items.map(function(item, index){
            return <CMenu key={index} menu={item}></CMenu>
        })
        return (  
            <>
            <li>
                <a href={strMenuHRef} data-toggle="collapse" aria-expanded="false" className="dropdown-toggle">
                    <i className={menu.icon}></i>
                    {menu.name}
                </a>
                <ul className="collapse list-unstyled" id={strSubMenuId}>
                    {subMenus}
                </ul>
            </li>
            </>     
        );
    }
}

export default CMenuWithSubMenu;


