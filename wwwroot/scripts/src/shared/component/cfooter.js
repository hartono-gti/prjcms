﻿import React from 'react';
import ReactDOM from 'react-dom';
import footerstyle from './cfooter.module.scss'; // using module technique

const CFooter = () => 
    <div className={footerstyle.footer}>Copyright © 2020</div>;

export default CFooter;
