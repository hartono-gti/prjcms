﻿import React from 'react';
import ReactDOM from 'react-dom';

class CMenu extends React.Component {
    constructor(props) {
        super(props);
    }    
        
    render(){
        const menu = this.props.menu
        return (
            <>
            <li>
                <a href={menu.url}>
                    <i className={menu.icon}></i>
                    {menu.name}
                </a>
            </li>
           </>    
        );
    }
}

export default CMenu;


