﻿import React from 'react';
import ReactDOM from 'react-dom';
import "./csidebar.scss";
import CMenu from "./cmenu";
import CMenuWithSubMenu from "./cmenuWithSubmenu";

class CSideBar extends React.Component {
    constructor(props) {
        super(props);
    }    
        
    render() {
        const menus = this.props.menus.map(function(menu, index){
            if(menu.items.length==0)
                return <CMenu key={index} menuId={index} menu={menu}></CMenu>;
            else
                return <CMenuWithSubMenu key={index} menuId={index} menu={menu}></CMenuWithSubMenu>;
        })

        return (
        <>                        
            <ul className="list-unstyled">
                {menus}                
            </ul>
        </>
        );
    }
}

export default CSideBar;


