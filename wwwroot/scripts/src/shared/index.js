﻿import React from 'react';
import ReactDOM from 'react-dom';
import CHeader from './component/cheader';
import CFooter from './component/cfooter';
import CSideBar from './component/csidebar';

import jquery from "jquery";
window.$ = jquery;
window.jQuery = jquery

import 'bootstrap';
import '../../../styles/bootstrap-custom.scss';
import "../../../styles/font-awesome.scss";
import dt from "datatables.net";
import "datatables.net-bs4";

var menus = [];
if (gRoleId == "1") {
    menus =
        [
            {
                name: 'Users',
                url: '/User/Index',
                icon: 'fa fa-group',
                items: []
            },
            {
                name: 'Shape Categories',
                url: '/ShapeCategory/Index',
                icon: 'fa fa-square-o',
                items: []
            },
            {
                name: 'User Shapes',
                url: '/UserShape/Index',
                icon: 'fa fa-square-o',
                items: []
            }
        ];
} else {
   menus =
        [
            {
                name: 'User Shapes',
                url: '/UserShape/Index',
                icon: 'fa fa-info-circle',
                items: []
            }
        ];
}


ReactDOM.render(<CHeader/>,document.getElementById('header'));
ReactDOM.render(<CSideBar menus={menus} />, document.getElementById('side'));
ReactDOM.render(<CFooter />, document.getElementById('footer'));

